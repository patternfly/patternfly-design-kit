# PatternFly Design Kit
**The PatternFly Design Kit** is a collection of Figma components that make it easy for designers to create low and high-fidelity design mockups that accurately represent the [PatternFly](http://patternfly.org) components and layouts.

The PatternFly 6 design kit gives you access to:

* Our complete component library.
* Our design token system.
* Demos and patterns of full-screen designs.

The figma design kit includes:

* **[PatternFly 6: Component library](https://www.figma.com/community/file/1357060119827689328/patternfly-6-components)** – includes all standard components and icons
* **[PatternFly 6: Patterns & Extensions Library](https://www.figma.com/community/file/1357062621908564852/patternfly-6-patterns-extensions)** – provides several patterns and demos that solve common UI problems by grouping multiple PatternFly components into reusable layouts
* **[PatternFly 6: Design Tokens & Styles](https://www.figma.com/community/file/1357058122244711273/patternfly-6-design-tokens-styles)** – contains all the variables for color, spacing and typography that convey the associated design style for PatternFly

## Updating the kit
The PatternFly team will release updates accordingly as part of their sprint schedule. Be sure to revisit our [Figma community page](https://www.figma.com/@patternfly) in order to download the latest updates.
If you are working in the main community file, Figma will automatically receive library updates. If you are working from a local file, you will not receive automatic updates and will need to update the PatternFly library manually.


## Filing bugs
If you believe that you've come across a PatternFly Design Kit bug, alert our team, so that we can resolve the issue. To report a bug, follow these steps:
1. View the documentation for the feature to confirm that the behavior is not functioning as intended. Friendly Reminder: We try our best to match the component within Figma to what is represented on patternfly.org. Double check to verify that this is a Figma design kit bug vs. a general design enhancement.
2. Search open issues in this repo to see if a related issue already exists.
3. File an issue in this repo and label it with "bug". If something is wrong with any of the Figma libraries, please contact Lucia Boehling, Kayla Chumley or Bekah Stephens.


## Contributing to the kit
We welcome contributions from our community of PatternFly designers. Reach out to the team within the [patternfly.slack.com workspace](http://patternfly.slack.com) via the #patternfly-design-kit channel, and our team will assist with questions on design kit enhancements.

![Component Contributing guide](https://github.com/user-attachments/assets/f7c32a9b-76ab-457b-b330-08bc861cbb5f)

### Contributing a new or edited component, pattern, or extension
Follow these steps:
1. Create a copy of the PF6 Design Kit Figma file you are working from or create a new branch hosted in Figma.
2. If the concept you're working on fits within an existing design, add it to that existing component/pattern/layout page tab. If it doesn't fit into an existing component/pattern/layout, create a new page and then start fresh from there.
3. Copy a link to your Figma file or branch and attach it to the GitHub issue it is related to. Add your contribution or enhancement topic to the [PatternFly & UXD Design Share agenda](https://docs.google.com/document/d/1sZDSHsC5LZM1-7EvoYF4idji83TcZ7FargqRuifai_8/edit?tab=t.0#heading=h.cmjge5yxw5ee). Discuss your contribution idea with the broader team. Be sure to send a Slack message to the PatternFly design team for additional support.

### Questions?
If you have any questions or concerns, please contact Lucia Boehling, Kayla Chumley, Bekah Stephens, or Andrew Ronaldson.
